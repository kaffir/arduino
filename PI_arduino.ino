#include<LiquidCrystal.h>

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);     // select the pins used on the LCD panel
#define btnRIGHT  0
#define btnUP     1
#define btnDOWN   2
#define btnLEFT   3
#define btnSELECT 4
#define btnNONE   5

// Initialisation des variables

int BUTTON_PIN = A0;
int timer, cpt_min, rec, holdMin, holdChrono = 0;
int adc_key_in , lcd_key, cpt_heure = 0;
int chrono = 1000;
int rtRand, crRand, shRand  ;
void display();
void Send();
long prevRand , erreurRand, randNumber, initRand;
long ardChrono = 1;
String weather, tram , holdTram;
String holdWeather = "sunny_cloudy";
bool etatButton;

int read_LCD_buttons() {                           // Lire les bouttons
  adc_key_in = analogRead(0);                      // Lire les valeurs du capteur

  if (adc_key_in > 1000) return btnNONE;
  if (adc_key_in < 50)   return btnRIGHT;
  if (adc_key_in < 250)  return btnUP;
  if (adc_key_in < 450)  return btnDOWN;
  if (adc_key_in < 650)  return btnLEFT;
  if (adc_key_in < 850)  return btnSELECT;
  return btnNONE;
}
////// Machine a état horloge //////
typedef enum {
  HORLOGE_IDLE,
  HORLOGE_MIN,
  HORLOGE_HEURE
} HORLOGE_STATE;

HORLOGE_STATE hstate;

void HORLOGE_init()
{
  hstate = HORLOGE_IDLE;
}

void HORLOGE_update()
{

  HORLOGE_STATE nextState = hstate;
  switch (hstate) {
    case HORLOGE_IDLE:
      if (timer >= chrono) {            // permet de rajouter une minute au compteur quand le timer depasse 1000 ms
        nextState = HORLOGE_MIN;
      }
      break;

    case HORLOGE_MIN:

      if (cpt_min != 59) {
        nextState = HORLOGE_IDLE;
      }
      if (cpt_min == 59) {
        nextState = HORLOGE_HEURE;
      }
      break;

    case HORLOGE_HEURE:
      if (cpt_heure != 24) {
        nextState = HORLOGE_IDLE;
      }
      if (cpt_heure == 24) {
        nextState = HORLOGE_IDLE;
      }
      break;

  } hstate = nextState;
}
void HORLOGE_output()
{
  switch (hstate) {
    case HORLOGE_IDLE:
      timer = millis();
      break;
    case HORLOGE_MIN:
      chrono = millis() + 1000 ;
      if (cpt_min == 59) {
        cpt_min = 0;
      }
      display();
      holdMin = cpt_min;
      cpt_min++;
      break;
    case HORLOGE_HEURE:
      cpt_heure++;

      if (cpt_heure % 12 == 0) {        //Permet d'envoyer la métèo toute les douze heure
        WEATHER_update();
        Send();
      }
      break;
  }
}

////// Machine a état Métèo //////
typedef enum {
  WEATHER_IDLE,
  WEATHER_T,
  WEATHER_R,
  WEATHER_C,
  WEATHER_S,
  WEATHER_H
} WEATHER_STATE;

WEATHER_STATE weatherState;


void WEATHER_init()
{
  weatherState = WEATHER_IDLE;

}

void WEATHER_update()
{
  if (cpt_heure % 12 == 0) {
    WEATHER_STATE prevState = weatherState;

    switch (weatherState) {
      case WEATHER_IDLE:


        if ( initRand <= 5 ) {
          prevState = WEATHER_T;
        }
        if ( 6 <= initRand && initRand <= 20 ) {
          prevState = WEATHER_R;
        }
        if ( 21 <= initRand && initRand <= 40 ) {
          prevState = WEATHER_C;
        }
        if ( 41 <= initRand && initRand <= 80  ) {
          prevState = WEATHER_S;
        }
        if ( 81 <= initRand && initRand <= 100  ) {
          prevState = WEATHER_H;
        }
        break;

      case WEATHER_T:

        if (erreurRand <= 88 ) {
          prevState = WEATHER_R;
          weather = "thunder_rainy";
        }
        if ( 89 <= erreurRand && erreurRand <= 98) {
          weather = "thunder_rainy";
          prevState = WEATHER_C;
        }
        if ( 99 <= erreurRand ) {
          prevState = WEATHER_IDLE;
        }
        break;

      case WEATHER_R:
        if ( erreurRand <= 88 ) {
          if ( rtRand <= 5 ) {
            prevState = WEATHER_T;
            weather = "rainy_thunder";
          } else {
            prevState = WEATHER_C;
            weather = "rainy_cloudy";
          }
        }
        if ( 89 <= erreurRand && erreurRand <= 98) {
          weather = "rainy_cloudy";
          prevState = WEATHER_S;
        }
        if ( 99 <= erreurRand ) {
          prevState = WEATHER_IDLE;
        }
        break;

      case WEATHER_C:
        if ( erreurRand <= 88 ) {
          if ( crRand <= 15 ) {
            prevState = WEATHER_R;
            weather = "cloudy_rainy";
          } else {
            prevState = WEATHER_S;
            weather = "cloudy_sunny";
          }
        }
        if ( 89 <= erreurRand && erreurRand <= 98) {
          if ( rtRand <= 5 ) {
            prevState = WEATHER_T;
            weather = "cloudy_rainy";
          } else {
            prevState = WEATHER_H;
            weather = "cloudy_sunny";
          }
        }
        if ( 99 <= erreurRand ) {
          prevState = WEATHER_IDLE;
        }
        break;

      case WEATHER_S:
        if ( erreurRand <= 88 ) {
          if ( shRand == 1) {
            prevState = WEATHER_H;
            weather = "sunny_heatwave";
          } else {
            prevState = WEATHER_C;
            weather = "sunny_cloudy";
          }
        }
        if ( 89 <= erreurRand && erreurRand <= 98) {
          weather = "sunny_cloudy";
          prevState = WEATHER_R;
        }
        if ( 99 <= erreurRand ) {
          prevState = WEATHER_IDLE;
        }
        break;
      case WEATHER_H:
        if ( erreurRand <= 88 ) {
          prevState = WEATHER_S;
          weather = "heatwave_sunny";
        }
        if ( 89 <= erreurRand && erreurRand <= 98) {
          weather = "heatwave_sunny";
          prevState = WEATHER_C;
        }
        if ( 99 <= erreurRand ) {
          prevState = WEATHER_IDLE;
        }
        break;

    } weatherState = prevState;
  }
}
void WEATHER_output() {
  switch (weatherState) {
    case WEATHER_T:
      if (erreurRand <= 88 ) {
        weather = "thunder_rainy";
      }
      if ( 89 <= erreurRand && erreurRand <= 98) {
        weather = "thunder_rainy";
      }
      break;
    case WEATHER_R:
      if ( erreurRand <= 88 ) {
        if ( rtRand <= 5 ) {
          weather = "rainy_thunder";
        } else {
          weather = "rainy_cloudy";
        }
      }
      if ( 89 <= erreurRand && erreurRand <= 98) {
        weather = "rainy_cloudy";
      }
      break;
    case WEATHER_C:
      if ( erreurRand <= 88 ) {
        if ( crRand <= 15 ) {
          weather = "cloudy_rainy";
        } else {
          weather = "cloudy_sunny";
        }
      }
      if ( 89 <= erreurRand && erreurRand <= 98) {
        if ( rtRand <= 5 ) {
          weather = "cloudy_rainy";
        } else {
          weather = "cloudy_sunny";
        }
      }
      break;

    case WEATHER_S:
      if ( erreurRand <= 88 ) {
        if ( shRand == 1) {
          weather = "sunny_heatwave";
        } else {
          weather = "sunny_cloudy";
        }
      }
      if ( 89 <= erreurRand && erreurRand <= 98) {
        weather = "sunny_cloudy";
      }
      break;
    case WEATHER_H:
      if ( erreurRand <= 88 ) {
        weather = "heatwave_sunny";
      }
      if ( 89 <= erreurRand && erreurRand <= 98) {
        weather = "heatwave_sunny";
      }
      break;
  }
}

////// Machine a état boutton //////
typedef enum {
  BUTTON_ACC,
  BUTTON_DCC,
  BUTTON_IDLE
} BUTTON_STATE;

BUTTON_STATE bState;

void BUTTON_init() {
  bState = BUTTON_IDLE;
  pinMode(BUTTON_PIN, INPUT);
  etatButton = false;
}

void BUTTON_update() {
  BUTTON_PIN = analogRead(A0);
  etatButton = digitalRead(BUTTON_PIN);
  BUTTON_STATE nextState = bState;
  switch (bState) {

    case BUTTON_IDLE:
      if (BUTTON_PIN < 50) {
        nextState = BUTTON_ACC;
      }
      if (BUTTON_PIN >= 450 && BUTTON_PIN < 650) {
        nextState = BUTTON_DCC;
      }
      break;
    case BUTTON_ACC:
      nextState = BUTTON_IDLE;
      break;
    case BUTTON_DCC:
      nextState = BUTTON_IDLE;
      break;
  } bState = nextState;
}
void BUTTON_output() {
  switch (bState) {
    case BUTTON_IDLE:
      holdChrono = chrono;
      break;
    case BUTTON_ACC:
      chrono -= 400;
      lcd.setCursor(8, 0);
      lcd.print(">>");
      break;
    case BUTTON_DCC:
      chrono += 50 ;
      if (chrono <= 20000) {
        chrono = 20000;
      }
      lcd.setCursor(8, 0);
      lcd.print("<<");
      break;
  }
}
////// Fonction d'envoie ou d'affichage //////
void Send() {
  Serial.println((String)"[" + cpt_heure + "_" + weather + "]"); //Envoi unr trame dece type : [0_sunny_cloudy]
  holdWeather = weather;
}
void display() {
  lcd.clear();
  lcd.setCursor(0, 1);
  lcd.print(holdWeather);
  lcd.setCursor(1, 0);
  lcd.print(cpt_heure);
  lcd.print("h");
  lcd.print(cpt_min);
}
void setup() {

  Serial.begin(9600);
  HORLOGE_init();
  WEATHER_init();
  BUTTON_init();
  randomSeed(analogRead(ardChrono));
  lcd.begin(16, 2);

}

void loop() {
  HORLOGE_update();
  HORLOGE_output();
  WEATHER_update();
  WEATHER_output();
  BUTTON_update();
  BUTTON_output();
  /////Permet de ne pas envoyer deux trame identiques/////
  tram = (String)"[" + cpt_heure + "_" + holdWeather + "]";
  if ( holdTram != tram ) {
    Serial.println((String)"[" + cpt_heure +  "_" + holdWeather + "]");
    holdTram = (String)"[" + cpt_heure + "_" + holdWeather + "]" ;
  }
  ///////////////////////////////////////////////////
  initRand = random(1, 6);
  ardChrono += millis();
  prevRand = random(1, 3);
  erreurRand = random(1, 101);
  rtRand = random(1, 26);
  crRand = random(1, 35);
  shRand = random(1, 3);
}



